# Chat cliente servidor a nivel web

## Paso a paso para su ejecución 

````Bash

sudo su
apt update
apt upgrade -y
apt install nginx -y 
nano /etc/nginx/sites-available/default
# Se pega el archivo nginx_configuration suministrado
# Verificamos si está bien la configuración ingresada
nginx -t
service nginx restart
curl -sL https://deb.nodesource.com/setup_20.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt install nodejs -y
apt install build-essential
npm install pm2@latest -g
cd /var/www/html
mkdir chatapp
cd chatapp
npm init
npm install -s express
npm install -s body-parser
npm install -s socket.io
npm install -s http
#creamos el archivo de index.html
nano index.html
#creamos el archivo index.js
nano index.js
# Levantamos el servicio como demonio del archivo index.js
pm2 start index.js
# Levantamos el servicio pm2 de ejecución de entorno como auto-boot
m2 startup systemd
# Se guardan los cambios
pm2 save
# Iniciamos el servicio "Padre" con permisos de administrador
systemctl start pm2-root
````
Luego copiamos y pegamos la IP de la instancia en google y damos enter para acceder
